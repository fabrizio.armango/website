import React, { useCallback, useEffect, useRef, useState } from "react";
import cn from "clsx";

type CookieConsent = {
  necessary: boolean;
  analytics: boolean;
  preferences: boolean;
  marketing: boolean;
};

function saveConsent(consent: CookieConsent) {
  const consentMode = {
    functionality_storage: consent.necessary ? "granted" : "denied",
    security_storage: consent.necessary ? "granted" : "denied",
    ad_storage: consent.marketing ? "granted" : "denied",
    analytics_storage: consent.analytics ? "granted" : "denied",
    personalization: consent.preferences ? "granted" : "denied",
  };
  // @ts-ignore
  gtag("consent", "update", consent);
  localStorage.setItem("consentMode", JSON.stringify(consentMode));
}

export const CookieConsentBanner = () => {
  const [currentConsent, setCurrentConsent] = useState<CookieConsent | null>(
    null,
  );

  const [showBanner, setShowBanner] = useState(false);

  const consentAnalyticsRef = useRef<HTMLInputElement>(null);
  const consentPreferencesRef = useRef<HTMLInputElement>(null);
  const consentMarketingRef = useRef<HTMLInputElement>(null);

  const btnAcceptAllClickHandler = useCallback(() => {
    setCurrentConsent({
      necessary: true,
      analytics: true,
      preferences: true,
      marketing: true,
    });
    setShowBanner(false);
  }, []);

  const btnAcceptSomeClickHandler = useCallback(() => {
    if (!consentAnalyticsRef.current) return;
    if (!consentPreferencesRef.current) return;
    if (!consentMarketingRef.current) return;

    setCurrentConsent({
      necessary: true,
      analytics: consentAnalyticsRef.current.checked,
      preferences: consentPreferencesRef.current.checked,
      marketing: consentMarketingRef.current.checked,
    });
    setShowBanner(false);
  }, []);

  const btnRejectAllClickHandler = useCallback(() => {
    setCurrentConsent({
      necessary: false,
      analytics: false,
      preferences: false,
      marketing: false,
    });
    setShowBanner(false);
  }, []);

  useEffect(() => {
    const localConsentMode = localStorage.getItem("consentMode");
    setShowBanner(localConsentMode == null);
    setCurrentConsent(localConsentMode ? JSON.parse(localConsentMode) : null);
  }, []);

  useEffect(() => {
    if (!currentConsent) return;
    saveConsent(currentConsent);
  }, [currentConsent]);

  return (
    <div
      className={cn({
        hidden: !showBanner,
        block: showBanner,
        "p-4 text-center border-t": true,
        "fixed bottom-0 left-0 right-0": true,
        "z-[1000] bg-[#f8f9fa]": true,
      })}
    >
      <h3 className="text-2xl font-bold">Cookie settings</h3>
      <p className="p-4">
        We use cookies to provide you with the best possible experience. They
        also allow us to analyze user behavior in order to constantly improve
        the website for you.
      </p>
      <button
        id="btn-accept-all"
        className="border-0 py-2 px-4 text-center text-decoration-none inline-block text-lg m-2 cursor pointer rounded bg-[#34a853] text-white cookie-consent-button btn-success bg-blue-800"
        onClick={btnAcceptAllClickHandler}
      >
        Accept All
      </button>
      <button
        id="btn-accept-some"
        className="border-0 py-2 px-4 text-center text-decoration-none inline-block text-lg m-2 cursor pointer rounded bg-[#e6f4ea] text-[#34a853] cookie-consent-button btn-outline"
        onClick={btnAcceptSomeClickHandler}
      >
        Accept Selection
      </button>
      <button
        id="btn-reject-all"
        className="border-0 py-2 px-4 text-center text-decoration-none inline-block text-lg m-2 cursor pointer rounded bg-[#dfe1e5] text-black cookie-consent-button btn-grayscale"
        onClick={btnRejectAllClickHandler}
      >
        Reject All
      </button>
      <div className="flex justify-center items-center flex-wrap mb-4 cookie-consent-options">
        <label className="my-0 mx-4 text-lg">
          <input
            className="mr-2"
            type="checkbox"
            value="Necessary"
            defaultChecked
            disabled
          />
          Necessary
        </label>
        <label className="my-0 mx-4 text-lg">
          <input
            className="mr-2"
            type="checkbox"
            value="Analytics"
            defaultChecked
            ref={consentAnalyticsRef}
          />
          Analytics
        </label>
        <label className="my-0 mx-4 text-lg">
          <input
            className="mr-2"
            type="checkbox"
            value="Preferences"
            defaultChecked
            ref={consentPreferencesRef}
          />
          Preferences
        </label>
        <label className="my-0 mx-4 text-lg">
          <input
            className="mr-2"
            type="checkbox"
            value="Marketing"
            ref={consentMarketingRef}
          />
          Marketing
        </label>
      </div>
    </div>
  );
};
