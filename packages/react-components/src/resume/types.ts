export type ResumeData = {
  personInfo: {
    name: string;
    jobTitle: string;
    website: {
      url: string;
      text: string;
    };
    birthPlace: {
      city: string;
      country: string;
    };
    contacts: {
      email: string;
    };
  };
  education: {
    title: string;
    programs: {
      organization: {
        type: string;
        name: string;
        sameAs: string;
      };
      credential: {
        category: string;
        status: string;
        temporal: string;
        educationalLevelDescription: string;
        educationalLevel: string;
        url?: string;
      };
      courses: (string | { name: string; url?: string })[];
    }[];
  };
  workHistory: {
    title: string;
    jobs: {
      role: string;
      startDate: string;
    }[];
  };
};
