import React from "react";
import { Accordion } from "../accordion";
import type { ResumeData } from "./types";

export const Resume = (props: { data: ResumeData }) => {
  const { data } = props;
  return (
    <section
      itemScope
      itemType="http://schema.org/Person"
      className="lg:max-w-[1280px]"
    >
      <div>
        <div>
          <h3 className="font-bold text-2xl" itemProp="name">
            {data.personInfo.name}
          </h3>
          <h5 className="ml-5 italic mt-2" itemProp="jobTitle">
            {data.personInfo.jobTitle}
          </h5>
          <a itemProp="url" href={data.personInfo.website.url} hidden>
            {data.personInfo.website.text}
          </a>
          <p className="mt-10 text-gray-600">
            <small
              itemProp="birthPlace"
              itemScope
              itemType="https://schema.org/City"
            >
              <span itemProp="name">{data.personInfo.birthPlace.city}</span>
              <span>, </span>
              <span
                itemProp="containedInPlace"
                itemScope
                itemType="https://schema.org/State"
              >
                <span itemProp="name">Sicily</span>
                <span>, </span>
                <span
                  itemProp="containedInPlace"
                  itemScope
                  itemType="https://schema.org/Country"
                >
                  <span itemProp="name">
                    {data.personInfo.birthPlace.country}
                  </span>
                </span>
              </span>
            </small>
          </p>
          <small>
            <a
              href={`mailto:${data.personInfo.contacts.email}`}
              itemProp="email"
            >
              {data.personInfo.contacts.email}
            </a>
          </small>
        </div>
      </div>
      <div className="mt-20">
        <Accordion
          sections={[
            {
              header: data.workHistory.title,
              body: data.workHistory.jobs.map((workData, workDataIdx) => {
                return (
                  <div
                    key={workDataIdx}
                    itemProp="hasOccupation"
                    itemScope
                    itemType="https://schema.org/Role"
                    style={{
                      display: "flex",
                      width: "100%",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <span className="italic" itemProp="roleName">
                      {workData.role}
                    </span>
                    <small>
                      <span itemProp="startDate">{workData.startDate}</span>
                      <span> - Present</span>
                    </small>
                  </div>
                );
              }),
            },
            {
              header: data.education.title,
              body: (
                <ul className="grid gap-y-20">
                  {data.education.programs.map((educationalProgram, idx) => {
                    return (
                      <li key={idx}>
                        <div
                          itemProp="alumniOf"
                          itemScope
                          itemType={`https://schema.org/${educationalProgram.organization.type}`}
                          style={{
                            position: "relative",
                          }}
                        >
                          <div itemProp="name">
                            {educationalProgram.organization.name}
                          </div>
                          <link
                            itemProp="sameAs"
                            href={educationalProgram.organization.sameAs}
                          />
                          <div
                            itemScope
                            itemType="https://schema.org/EducationalOccupationalProgram"
                          >
                            <div
                              className="mb-10"
                              itemProp="educationalCredentialAwarded"
                              itemScope
                              itemType="https://schema.org/EducationalOccupationalCredential"
                            >
                              <div>
                                <i>
                                  {educationalProgram.credential.url ? (
                                    <a href={educationalProgram.credential.url}>
                                      <span itemProp="credentialCategory">
                                        {educationalProgram.credential.category}
                                      </span>
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="currentColor"
                                        className="bi bi-box-arrow-up-right ms-2"
                                        viewBox="0 0 16 16"
                                      >
                                        <path
                                          fillRule="evenodd"
                                          d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"
                                        />
                                        <path
                                          fillRule="evenodd"
                                          d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"
                                        />
                                      </svg>{" "}
                                    </a>
                                  ) : (
                                    <span itemProp="credentialCategory">
                                      {educationalProgram.credential.category}
                                    </span>
                                  )}
                                </i>
                              </div>
                              <p
                                className="mb-0"
                                style={{ textAlign: "right" }}
                              >
                                <small
                                  style={{
                                    fontSize: "0.75rem",
                                  }}
                                >
                                  {educationalProgram.credential.status}
                                  <span className="ms-1" itemProp="temporal">
                                    {educationalProgram.credential.temporal}
                                  </span>
                                </small>
                              </p>
                              <p
                                className="mb-0"
                                style={{ textAlign: "right" }}
                              >
                                <small
                                  style={{
                                    fontSize: "0.75rem",
                                  }}
                                >
                                  {
                                    educationalProgram.credential
                                      .educationalLevelDescription
                                  }
                                  <span itemProp="educationalLevel">
                                    {
                                      educationalProgram.credential
                                        .educationalLevel
                                    }
                                  </span>
                                </small>
                              </p>
                            </div>
                            <Accordion
                              sections={[
                                {
                                  header: "Courses",
                                  body: (
                                    <ul className="text-sm divide-y text-gray-700 text-sm">
                                      {educationalProgram.courses.map(
                                        (course, courseIdx) => {
                                          let courseName: string;
                                          if (typeof course == "string") {
                                            courseName = course;
                                            return (
                                              <li
                                                key={courseIdx}
                                                className="py-2"
                                              >
                                                <span itemProp="hasCourse">
                                                  {courseName}
                                                </span>
                                              </li>
                                            );
                                          } else {
                                            courseName = course.name;
                                            if (course.url)
                                              return (
                                                <li
                                                  key={courseIdx}
                                                  className="py-2"
                                                >
                                                  <a href={course.url}>
                                                    <span itemProp="hasCourse">
                                                      {courseName}
                                                    </span>
                                                    <svg
                                                      xmlns="http://www.w3.org/2000/svg"
                                                      width="16"
                                                      height="16"
                                                      fill="currentColor"
                                                      className="bi bi-box-arrow-up-right ms-2"
                                                      viewBox="0 0 16 16"
                                                    >
                                                      <path
                                                        fillRule="evenodd"
                                                        d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"
                                                      />
                                                      <path
                                                        fillRule="evenodd"
                                                        d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"
                                                      />
                                                    </svg>{" "}
                                                  </a>
                                                </li>
                                              );
                                          }
                                        },
                                      )}
                                    </ul>
                                  ),
                                },
                              ]}
                            />
                          </div>
                        </div>
                      </li>
                    );
                  })}
                </ul>
              ),
            },
          ]}
        />
      </div>
    </section>
  );
};
