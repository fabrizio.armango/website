import React, { ReactNode, useState } from "react";
import cn from "clsx";

export const Accordion = ({
  sections,
}: {
  sections: {
    header: ReactNode;
    body: ReactNode;
  }[];
}) => {
  const [openSections, setOpenSections] = useState<{
    [idx: number]: boolean;
  }>({});
  return (
    <div>
      {sections.map((section, sectionIdx) => {
        return (
          <div
            key={sectionIdx}
            className="border divide-y relative overflow-hidden"
          >
            <div
              onClick={() => {
                setOpenSections({
                  ...openSections,
                  [sectionIdx]: !openSections?.[sectionIdx],
                });
              }}
              className="p-6 font-bold text-md text-gray-900 hover:bg-gray-100 cursor-pointer z-[1]"
            >
              {section.header}
            </div>
            <div
              className={cn({
                "transition-[height] delay-150 ease-in-out": true,
                "p-10 opacity-1 h-fit": openSections?.[sectionIdx],
                "opacity-0 h-0 pointer-events-none":
                  !openSections?.[sectionIdx],
              })}
            >
              {section.body}
            </div>
          </div>
        );
      })}
    </div>
  );
};
