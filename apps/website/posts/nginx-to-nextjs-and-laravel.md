---
title: "nginx-to-nextjs-and-laravel"
description: "NGINX config file to serve static NextJS frontend files and proxy API requests to Laravel endpoint using php-fpm."
date: "2022-10-11"
labels: "side-project, stack::javascript, stack::laravel, stack::nginx, stack::docker-compose, stack::php, stack::nextjs"
articleUrl: "/notes/nginx-to-nextjs-and-laravel"
thumbnail: null
isGitlab: true
repository: "https://gitlab.com/fabrizioarmango/nginx-to-nextjs-and-laravel"
---

Link: [https://gitlab.com/fabrizioarmango/nginx-to-nextjs-and-laravel](https://gitlab.com/fabrizioarmango/nginx-to-nextjs-and-laravel)
