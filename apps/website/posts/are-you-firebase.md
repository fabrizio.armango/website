---
title: "Firestore. Are you Firebase?"
date: "2022-07-10"
labels: "stack::firestore, stack::firebase, stack::orchestra.software, dev-diary"
---

It's been five years since I played Firebase last time. It was a good mate in my "openpa" project which I implemented for my OpenData exam.
A simple project composed by:

- 2 open datasets provided by the Municipality of Palermo (turism and theatres)
- a web application in React (exact that sensation described in memes when you see code you wrote many years ago.thats)
- [three python microservices/webhooks](https://github.com/FabrizioArmango/openpa/tree/master/microservices) relying on hook.io.
  At the time of the project, November 2017, hook.io was a platform letting you to have a public endpoint executing the code in a file located on a GitHub repository.
  Very useful and simple to setup, you just had to select the language and provide it with the url to the source file, and it offered a totally free plan choice.

  Today it no more exists (please let me know if i'm wrong).

  > Curiosity: I checked the first release date of Google Functions and it happened in the same year: [**Beta Release on March 2017**](https://cloud.google.com/functions/docs/release-notes).

- A php backend to fetch webhooks and store results into Firebase.
- Python scripts to cross and transform the fetched data into a 5-star Linked Data Graph in a custom ontology in RDF\XML format!

Anyway at that time, being a frontend developer I really liked the possibility given by Firebase just to send a JSON object somewhere and retrieve it whenever i would without dealing with the complexity of lower layers.
No VPS to buy, MySQL installation, ER diagrams to prepare the database schema, no maintainance. Just a curl from PHP. It was amazing and the perfect choice for my exam project.

Now, laziness apart, Firestore seems a good and modern option and I want to consider it for storing data needed by the services which orchestra is composed by.
First of all, I want to start from creating [_Manifests_](https://orchestra.software/docs/manifest), the core idea Orchestra it's based on, which are logical containers to represent a software system/project.

## Setup Cloud Firestore for the "Manifest" entity in Orchestra.software

[Continue reading...](/notes/manifest-entity-orchestra)
