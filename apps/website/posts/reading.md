---
title: "Reading"
date: "2023-04-20"
labels: ""
---

I've decided to write this note listing the books and articles I want to read, so as not to miss them.

I divided them into three lists:

- **Waiting queue**, for those to buy/bought and to read;
- **In progress**, for those I'm currently reading;
- **Parsed**, for those already read, in case I want to remember some names for a quick check or to recommend them.

#### Waiting queue

- Technology Strategy Patterns: Architecture as Strategy, by Eben Hewitt
- [Making Things Happen](https://www.oreilly.com/library/view/making-things-happen/9780596517717/ch04.html)
- [How to Set the Technical Direction for Your Team](http://jlhood.com/how-to-set-team-technical-direction/)
- [Writing our 3-year technical vision](https://www.eventbrite.com/engineering/writing-our-3-year-technical-vision/)
- [Technical strategy power chords](https://leaddev.com/leaddev-live/technical-strategy-power-chords)
- [Getting to Commitment: Tackling broad technical problems in large organizations](https://leaddev.com/staffplus-new-york/video/getting-commitment-tackling-broad-technical-problems-large-organizations)
- [A survey of engineering strategies.](https://lethain.com/survey-of-engineering-strategies)
- [RFC 7282 - On Consensus and Humming in the IETF](https://datatracker.ietf.org/doc/html/rfc7282#page-4)
- [Debugging Teams](https://book.debuggingteams.com/#manipulating_your_organization)
- Fundamentals of Software Architecture: An Engineering Approach, by Neal Ford and Mark Richards

#### In progress

- The Staff Engineer's Path, by Tanya Reilly
- Intelligenza artificiale, by Sergio Parra and Marc Torrens

#### Parsed

- The Software Architect Elevator, by Gregor Hohpe

> // TO DO note for this note: add books of uni courses

> _Last update: April, 20, 2023_
