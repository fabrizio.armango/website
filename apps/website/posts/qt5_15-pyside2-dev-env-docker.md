---
title: "Qt5.15 + PySide2 Development environment in docker container"
date: "2022-03-08"
labels: "stack::docker, stack::qt5"
---

### Files

- {project_root}
  - [docker-compose.yml](#docker-compose.yml)
  - Dockerfile
  - run.sh

#### docker-compose.yml

```yaml
version: "3"
services:
  app:
    image: elsi-qt:5.15-desktop
    build: .
    command: sh -c "./root/run.sh"
    volumes:
      - $HOME/.Xauthority:/root/.Xauthority
      - /tmp/.X11-unix:/tmp/.X11-unix
      - ".:/root"
    environment:
      - DISPLAY=${DISPLAY}
    network_mode: host
```

`Dockerfile`

```Dockerfile

FROM ubuntu:20.04

RUN apt-get update

RUN apt-get -y install build-essential libgl1-mesa-dev

RUN apt-get -y install python3 python3-pip

RUN DEBIAN_FRONTEND='noninteractive' apt-get -y install libglib2.0-0 rsyslog-gssapi

RUN apt-get -y install libfontconfig


# Python packages required by the project
RUN pip3 install PySide2 qt-material

RUN apt-get -y install libxcb-icccm4
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-icccm.so.4: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-icccm.so.4: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxcb-image0
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-image.so.0: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-image.so.0: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxcb-keysyms1
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-keysyms.so.1: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-keysyms.so.1: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxcb-render-util0
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-render-util.so.0: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-render-util.so.0: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxcb-shape0
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-shape.so.0: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-shape.so.0: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxcb-xinerama0
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-xinerama.so.0: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-xinerama.so.0: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxcb-xkb-dev
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-xkb.so.1: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxcb-xkb.so.1: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libxkbcommon-x11-0
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxkbcommon-x11.so.0: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libxkbcommon-x11.so.0: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

RUN apt-get -y install libdbus-1-dev
# fixes:
# app_1  | Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libdbus-1.so.3: cannot open shared object file: No such file or directory)
# app_1  | QLibraryPrivate::loadPlugin failed on "/usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so" : "Cannot load library /usr/local/lib/python3.8/dist-packages/PySide2/Qt/plugins/platforms/libqxcb.so: (libdbus-1.so.3: cannot open shared object file: No such file or directory)"
# app_1  | qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

ENV QT_DEBUG_PLUGINS=1
```

`run.sh`

```bash
#!/bin/bash
python3 /root/app.py
```

### Run

```bash
xhost local:docker
docker-compose up
```

#### compile .ui to .py

```
pyuic5 form.ui -o ui.py
```

```
Traceback (most recent call last):
  File "/usr/lib/python3.8/runpy.py", line 194, in _run_module_as_main
    return _run_code(code, main_globals, None,
  File "/usr/lib/python3.8/runpy.py", line 87, in _run_code
    exec(code, run_globals)
  File "/usr/lib/python3/dist-packages/PyQt5/uic/pyuic.py", line 26, in <module>
    from PyQt5 import QtCore
ImportError: /usr/lib/python3/dist-packages/PyQt5/QtCore.cpython-38-x86_64-linux-gnu.so: undefined symbol: _Zpl14QDeadlineTimerx, version Qt_5
```

---

## hot reload version

Installing the package `inotify-tools` to the container we'll be able to watch source files and then automatically convert .ui files to .py files and run the program.

- Append this line to the Dockerfile
  `RUN apt-get -y install inotify-tools`
- create new file `scripts/hotdev.sh`

`scripts/hotdev.sh`

```bash
while true; do
    pyside2-uic /root/form.ui -o /root/ui.py
    python3 /root/app.py &

    PID=$!
    status=$?

    inotifywait -e modify -e move -e create -e delete -e attrib --exclude tmp -r .

    kill -9 $PID
done
```

- edit `run.sh`
  `run.sh`

```bash
#!/bin/bash
/root/scripts/hotdev.sh
```
