---
title: "Run Modelio on Ubuntu 20.04"
date: "2022-03-03"
labels: "stack::docker, snippet"
---

Last time I used Modelio was some year ago. I wanted to apply some change to UML diagrams so they could represent the updated system architecture...
When these projects have been created the version of my ubuntu was certainly earlier than 20.04 (my current desktop os). I discovered that Modelio cannot be run on it.

```
dpkg: dependency problems prevent configuration of modelio-open-source4.1:
    modelio-open-source4.1 depends on libwebkitgtk-1.0-0; however:
    Package libwebkitgtk-1.0-0 is not installed.
```

Trying to install it:

```
Package libwebkitgtk-1.0-0 is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'libwebkitgtk-1.0-0' has no installation candidate
```

I gave up but I didn't want to create them from scratch and would have preferred to recover the old workspaces/projects.
Therefore after I googled for a while I prepared the following files and snippet to run Modelio on a docker image created on top of Ubuntu 16.04.

`docker-compose.yml`

```
version: "3"

services:
 app:
   image: modelio:ubuntu16.04
   build: .
   volumes:
    - $HOME/.Xauthority:/root/.Xauthority
    - /tmp/.X11-unix:/tmp/.X11-unix
    - '.:/root'
   environment:
    - DISPLAY=${DISPLAY}
   network_mode: host
```

`Dockerfile`

```
FROM ubuntu:16.04
RUN apt-get update && apt-get install -y
RUN apt-get install -qqy x11-apps

COPY modelio-open-source_4.1.0_ubuntu_amd64.deb /root/modelio-open-source_4.1.0_ubuntu_amd64.deb
RUN dpkg -i /root/modelio-open-source_4.1.0_ubuntu_amd64.deb || true
RUN apt-get install -y -f

CMD ["sh", "-c", "/usr/lib/modelio-open-source4.1/modelio"]
```

### Usage

```
xhost local:docker
docker-compose build
docker-compose up
```
