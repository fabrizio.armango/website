---
title: "Bourne Shell?"
date: "2022-04-16"
labels: "stack::ash, stack::bash"
---

Today I got why at the top of a script is specified which shell to use.

I wrote a bash script for a ci/cd job, using an array specifying a list of "games" to be compiled. Everything works in local environment because I used an image built on top of "cypress/browsers:node16.13.2-chrome97-ff96" and with yarn installed.
Once I ran the script on the pipeline using the image "node:17-alpine the results were totally different!

First time:

```
/bin/sh: ./scripts/dist.sh: not found
```

Given that, I said: already encountered it, I've fixed it adding "#!/bin/sh" at the top, but...

Then:

```
./scripts/dist.sh: line 2: syntax error: unexpected "("
```

Let's google it... A funny guy defined the syntax I was using for defining an array `array=(a b c)` as " _a bashism_ ".
And going deeper... I have realized that my new (old) interpreter doesn't support arrays.
...
The same moment you asks yourself why haven't you written it in python, golang, node or whatelse....

So, how to replace this _bashism_ with a legal-sh syntax?

After a while I figured it out with the following translations.

#### Arrays

##### From

```
games=(id1 id2 id3 id4)
game_paths=(path1 path2 path3 path4)
for (( i=0; i<$n_games; i++ )); do
    game_id=${games[$i]}
    game_path=${games_paths[$i]}
done
```

##### To

```
game='id1 id2 id3 id4'
game_path_id1=path1
game_path_id2=path2
game_path_id3=path3
game_path_id4=path4

for game_id in $games; do
    eval game_path="\$game_path_$game_id"
```

#### Counters

##### From

```
num_succeeded=$((num_succeeded+1))
```

##### To

num_succeeded=`expr num_succeeded + 1`

#### Transform string to Uppercase

##### From

```
GAME_ID=${game_id^^}
```

##### To

```
GAME_ID=`expr $game_id | tr a-z A-z`
```

#### Variables with dynamic name:

##### From

```
is_current_game_enabled=IS_ENABLED_$GAME_ID
is_current_game_enabled=${!is_current_game_enabled}
```

##### To

```
eval is_current_game_enabled="\$IS_ENABLED_$GAME_ID"
```

#### Operators

##### From

```
if [ $num_succeeded > 0 ] && [ "$n_enabled_games" = "$num_succeeded" ]; then
```

##### To

```
if [ $num_succeeded -gt 0 -a $n_enabled_games -eq $num_succeeded ]; then
```

### Useful refs

- [Bourne Shell Reference](https://cis.stvincent.edu/html/tutorials/unix/bshellref)
