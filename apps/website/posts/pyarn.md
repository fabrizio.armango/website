---
title: "pyarn-boilerplate"
description: "Publish Python code as Yarn Package using GitLab CI/CD and Gitlab Package Registry."
date: "2022-08-03"
labels: "side-project, stack::typescript, stack::python, stack::python-shell, stack::docker, stack::gitlab-ci, stack::gitlab-package-registry"
articleUrl: "/notes/pyarn"
thumbnail: null
isGitlab: true
repository: "https://gitlab.com/fabrizioarmango/pyarn-boilerplate"
---

Some days ago I had an interesting talk with a friend about how to organize code.

We were wondering how to share python code among several nodejs/TypeScript projects and avoid manual copy and paste or the implementation of an updater.

Since the day before I had prepared a [boilerplate for a composer package](/notes/composer-package-boilerplate) for my colleague, a yarn package was the first stuff that I thought of!

That's it. The thing had intrigued me, seemed feasible and pretty simple.

Maybe I'll improve it in future.

At time of writing, the repo contains:

- **dev/linux/build.sh** -> a script to locally build the package in a docker container with node-alpine.
- **package.json** -> ready to build and publish new packages using yarn workspaces.
- **python-src-code** -> you won't believe it, but here you find the python code.
- **yarn_packages** -> I will surprise you: here the yarn packages. I've written a simple TypeScript function named `routineName` to wrap the execution of `main.py` as POC.

Ofc, the names of the folders `python-src-code`, `yarn_packages` can (must, please) be renamed.

Link: [https://gitlab.com/fabrizioarmango/pyarn-boilerplate](https://gitlab.com/fabrizioarmango/pyarn-boilerplate)
