---
title: "composer-package-boilerplate"
description: "Compile the frontend code using laravel-mix before publishing it as the Composer Package using GitLab CI/CD and Gitlab Package Registry."
date: "2022-08-03"
labels: "side-project, stack::javascript, stack::laravel-mix, stack::composer, stack::react, stack::gitlab-ci, stack::gitlab-package-registry"
articleUrl: "/notes/composer-package-boilerplate"
thumbnail: null
isGitlab: true
repository: "https://gitlab.com/fabrizioarmango/composer-package-boilerplate"
---

Link: [https://gitlab.com/fabrizioarmango/composer-package-boilerplate](https://gitlab.com/fabrizioarmango/composer-package-boilerplate)
