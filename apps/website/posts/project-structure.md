---
title: "CLI scripts and Project structure"
date: "2022-06-26"
labels: "stack::ash, stack::bash, stack::docker, stack::docker-compose, stack::npm, stack::nodejs, stack::yarn"
---

The aim of this issue is to have only docker-compose/docker as installed dependency to work locally on the project.
It could help our developers to work on the platform they prefer without applying changes on their local environment/workspace.
Furthermore, not using local versions of npm/yarn and node could avoid versioning issues.

Sometimes it could be a bit complex, working on several projects:

- do you have the same version of Node.js installed?
- are you running the command with the correct version of yarn/npm?
- have you globally installed something else that is interrupting the correct execution?

Or maybe after a while, you updated something which deprecates some dependencies of your project, or again: in ci/cd jobs do you use the same stack you use locally?

If you have already worked in Node.js, you'll find the following commands familiar:
`yarn start`, `yarn dev`, `npm run start`, `npm run build`, etc.
And that's amazing starting your project in development mode through a simple command or even better, to [make a build in one step](https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/#2).

## Start the project

So first of all, let's consider to replace `yarn start` with a `start.sh` script to be executed on linux, a `start.bat` or `start.ps1` to be executed on windows.
A new folder `dev` could help to divide scripts by os:

```
/dev
/dev/windows
/dev/windows/start.ps1
/dev/linux
/dev/linux/start.sh
```

### `/dev/linux/start.sh`

```
#!/usr/bin/env bash
BASEDIR=$(dirname "$0")
$BASEDIR/clean_node_modules.sh
docker-compose -p app -f $BASEDIR/../docker/docker-compose.start.yml up --remove-orphans --build
```

### `/dev/linux/start.sh`

```
#!/usr/bin/env bash
BASEDIR=$(dirname "$0")
$BASEDIR/clean_node_modules.sh
docker-compose -p app -f $BASEDIR/../docker/docker-compose.start.yml up --remove-orphans --build
```

### `/dev/windows/start.ps1`

```
$BASEDIR=$PSScriptRoot
docker-compose -f $BASEDIR/../docker/docker-compose.start.yml up --remove-orphans --force-recreate --build
```

### `/docker/docker-compose.start.yml`

```
version:          '3'

services:
  app:
    image:        node:18-alpine
    command:      sh -c "yarn install; yarn start"
```

### `/docker/docker-compose.test-dist.yml

```
version:          '3'

services:
  app-dist:
    build:
      context: .
      dockerfile: Dockerfile.dist
    command:      sh -c "yarn clean && yarn install && yarn dist && yarn test"
    working_dir:  /root/app
    volumes:
      - ../:/root/app:cached

  app-browse:
    image: nginx:1.21.6-alpine
    volumes:
      - ../:/app:cached
      #- ../docker/nginx_start.sh:/docker-entrypoint.d/nginx_start.sh
      - ../docker/nginx.conf:/etc/nginx/conf.d/default.conf
      - ../docker/nginx404.html:/usr/share/nginx/html/404.html
```

and it can be easily integrated into a gitlab pipeline

```
make_app_dist:
  stage: compile
  image: node:18-alpine
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - tmp-app/node_modules/
      # - .yarn
    policy: pull
  tags:
    - k8s-gitlab-runner
    - kubernetes
  script:
    - yarn run dist
    - mv packages/app/dist dist
  dependencies:
    - install_dependencies__app
  artifacts:
    paths:
      - tmp-app/dist
    reports:
      dotenv: dist.env
```

## How to install a yarn dependency inside the container?

Running: `./dev/linux/yarn_add.sh`

#### `/dev/linux/yarn_add.sh`

```
RUN="docker run \
    --env VAR_A=valueA \
    --env VAR_B=valueB \
    -v $(pwd):/root/app \
    -w /root/app \
    node:18-alpine3.15 \
    sh -c "

$RUN "yarn add <package_name> [options]"
```

# Project structure

```
/cypress
/decisions
/dev
/dev/windows
/dev/windows/start.ps1
/dev/linux
/dev/linux/start.sh
/dev/linux/yarn_add.sh
/docker
/packages
/releases
/releases/v1.0.0
/releases/v1.0.0/CHANGELOG.md
/webpack
/wiki

SCRIPTS.md
```
