---
title: "num-analysis"
description: "num-analysis.work"
date: "2023-04-04"
labels: "side-project, stack::typescript, stack::angular"
articleUrl: "/notes/numanalysis-demo"
thumbnail: ""
isGitlab: true
repository: "https://gitlab.com/fabrizioarmango/numanalysis-demo"
---

Live Demo: [https://num-analysis.work](https://num-analysis.work)
