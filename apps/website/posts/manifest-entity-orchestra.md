---
title: "Orchestra.software: The 'Manifest' entity."
date: "2022-07-11"
labels: "stack::firestore, stack::orchestra.software, dev-diary"
---

Firestore seems a good and modern option and I want to consider it for storing data needed by the services which orchestra is composed by.
First of all, I want to start from creating [_Manifests_](orchestra.software/docs/manifest), the core idea Orchestra it's based on, which are logical containers to represent a software system/project.

### What's the structure of a Manifest?

At this stage, I prefer focusing on essential features only and later use them for dogfooding and write down requirements for the Orchestra.software webapp through its user story management service.

That's why i want to keep things simple (The structure can be changed at any moment. Thanks Firebase).

A Manifest needs a name, a description, a list of useful links (for notion, gdrive, wikis) and something to show the user the linked user stories and release plans.

Since user stories are currently stored as issues in a specified GitLab repository, the manifest could have a list of repository urls (`userStoryStorages`).

Then release plans created by the [tool](https://orchestra.software/tools/sprints) are serialized in JSON and they could be stored in a releasePlans array of the Manifest.

To sum up:

```ts
class Manifest {
  name: string;
  description: string;
  urls: string[];
  // a list of urls to the
  // software/company's (live) documents
  // notion, drive documents, wikis

  userStoryStorages: string[];
  // A list of gitlab repositories
  // where issues representing user
  // stories are located.

  releasePlans: ReleasePlan[];
  visibility: "private" | "public";
}
```

I've added also the field `visibility`:

- if private is visible only to the user who created it.
- if public is visible to anyone.
- a new values "restricted" will be added later for custom sharing purposes.

### Golang implementation of the CreateManifest function

[What I aim to implement is a Google Function written in Golang to create a Manifest after that the user clicks the button "Create Manifest" in the `/manifest` page.](/notes/go-createmanifest-func)
