import { getSortedPostsData } from "./posts";

type Project = {
  id: string;
  title: string;
  labels: string;
  description: string;
  thumbnail?: string;
  articleUrl?: string;
  isGitlab?: boolean;
  repository?: string;
};

const ALLOWED_CHIPS = ["stack::"];

export function getSortedProjectsData() {
  const allProjectsData = (getSortedPostsData() as unknown as Project[])
    .filter((x) => x.labels.indexOf("side-project") !== -1)
    .filter((x) => ALLOWED_CHIPS.some((y) => x.labels.indexOf(y) !== -1))
    .map((x) => ({
      ...x,
      chips: x.labels
        .split(",")
        .filter((y) => y.indexOf("stack::") !== -1)
        .map((y) => {
          console.log(y);
          const tmp = y.trim().split("::");
          return {
            lead: tmp[0],
            value: tmp[1],
          };
        }),
    }));

  return allProjectsData;
}
