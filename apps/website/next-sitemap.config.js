/** @type {import('next-sitemap').IConfig} */
const nextSitemapConfig = {
	siteUrl: 'https://fabrizio.armango.it',
	generateRobotsTxt: true
}

module.exports = nextSitemapConfig