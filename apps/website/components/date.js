import { parseISO, format } from "date-fns";

export default function Date({ dateString, itemProp }) {
  const date = parseISO(dateString);
  return (
    <time dateTime={dateString} itemProp={itemProp}>
      {format(date, "LLLL d, yyyy")}
    </time>
  );
}
