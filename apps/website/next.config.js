/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  transpilePackages: ["@fabrizioarmango/react-components"],
  // assetPrefix: process.env.NODE_ENV === 'production' ? '/website' : '',
  output: "export",
  images: {
    loader: "akamai",
    path: "",
  },
};

module.exports = nextConfig;
