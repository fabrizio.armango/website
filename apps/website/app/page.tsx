import React from "react";
import Image from "next/image";
import id from "@fabrizioarmango/info/id.json";
import education from "@fabrizioarmango/info/education.json";
import workHistory from "@fabrizioarmango/info/work-history.json";

import { ResumeClient } from "./resume-client";

const FOOTER_DATA = {
  icons: [
    {
      src: "/images/gitlab-logo.svg",
      link: "https://gitlab.com/fabrizioarmango",
    },
    {
      src: "/images/github-mark.svg",
      link: "https://github.com/FabrizioArmango",
    },
    {
      src: "/images/logo-stackoverflow.svg",
      link: "https://stackoverflow.com/users/7055719/fabrizio-armango",
    },
  ],
};

export default function Home() {
  return (
    <>
      <main className="w-full">
        <div className="lg:max-w-[1280px] px-10 lg:mx-auto xl:px-0 py-20">
          <ResumeClient
            data={{
              education,
              personInfo: id,
              workHistory,
            }}
          />
        </div>
      </main>
      <footer className="lg:max-w-[1280px] mt-5 py-5 mx-auto">
        <nav
          className="grid grid-rows-1 grid-cols-3 text-center"
          aria-label="Footer"
        >
          {FOOTER_DATA.icons.map((icon, idx) => (
            <div key={idx} className="justify-center flex">
              <a href={icon.link}>
                <Image alt="" src={icon.src} width={36} height={36} />
              </a>
            </div>
          ))}
        </nav>
      </footer>
    </>
  );
}
