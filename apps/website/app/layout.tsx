import React, { ReactNode } from "react";
import Head from "next/head";
import Link from "next/link";
import styles from "./layout.module.css";
import type { Metadata } from "next";
import "../styles/global.css";
import { LayoutClient } from "./layout-client";

export const metadata: Metadata = {
  metadataBase: new URL("https://fabrizio.armango.it"),
  title: "Fabrizio Armango",
  description: "Software Engineer from Palermo",
  openGraph: {
    title: "Fabrizio Armango",
    description: "Software Engineer from Palermo",
    images: [
      {
        url: "https://dummyimage.com/1280x731/ffffff/000000.png&text=in+software",
      },
    ],
  },
  twitter: {
    card: "summary_large_image",
  },
  verification: {
    google: "TLAkA9C3T_NADy_YMISsrY_ldTrYtCZr6X12lKikmKY",
  },
};

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html>
      <head>
        <link rel="icon" href="/favicon.ico" />
      </head>
      <body>
        <div className={styles.container}>
          <header className="sticky top-0 z-[10] flex items-center p-3 mb-3 bg-white border-b drop-shadow">
            <Link className="p-2" href="/">
              ~
            </Link>
            <ul className="my-2 my-md-0 me-md-3 ms-auto list-inline flex">
              <li className="p-2 text-dark">
                <Link href="/projects">projects</Link>
              </li>
              <li className="p-2 text-dark ms-2">
                <Link href="/notes">notes</Link>
              </li>
            </ul>
          </header>
          {children}
        </div>
        <LayoutClient />
      </body>
    </html>
  );
}
