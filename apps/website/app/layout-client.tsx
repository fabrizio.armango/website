"use client";

import { CookieConsentBanner } from "@fabrizioarmango/react-components";
import Script from "next/script";

export const LayoutClient = () => {
  return (
    <>
      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-305N0ZLS0Z"
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}

          if(localStorage.getItem('consentMode') === null){
            gtag('consent', 'default', {
                'ad_storage': 'denied',
                'analytics_storage': 'denied',
                'personalization_storage': 'denied',
                'functionality_storage': 'denied',
                'security_storage': 'denied',
            });
          } else {
              gtag('consent', 'default', JSON.parse(localStorage.getItem('consentMode')));
          }
          
          gtag('js', new Date());

          gtag('config', 'G-305N0ZLS0Z');
        `}
      </Script>
      <CookieConsentBanner />
    </>
  );
};
