import { getSortedPostsData } from "../../lib/posts";
import Link from "next/link";
import Date from "../../components/date";

export default function Notes() {
  const allPostsData = getSortedPostsData();
  return (
    <main className="w-full">
      <section className="lg:max-w-[1280px] mx-auto">
        <h3 className="font-bold text-2xl text-center">Notes</h3>
        <div className="mt-4">
          <ul className="divide-y">
            {allPostsData.map(({ id, date, title, snippet, extras }) => (
              <li key={id} className="py-2 px-4">
                <div
                  className="flex w-full justify-between"
                  itemScope
                  itemType="https://schema.org/TechArticle"
                >
                  <Link href={`/notes/${id}`} className="link-primary">
                    <h5 className="mb-1" itemProp="name">
                      {title}
                    </h5>
                  </Link>
                  <small>
                    <Date dateString={date} itemProp="dateCreated" />
                  </small>
                </div>
                {snippet && <p className="mb-1">{snippet}</p>}
                {extras && <small>{extras}</small>}
              </li>
            ))}
          </ul>
        </div>
      </section>
    </main>
  );
}
