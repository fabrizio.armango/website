import { getAllPostIds, getPostData } from "../../../lib/posts";
import Image from "next/image";
import Date from "../../../components/date";

export const dynamicParams = true;
 
export async function generateStaticParams() {
  return getAllPostIds();
}
 
export default async function Post({params}) {
  const postData = await getPostData(params.id);

  return (
    <main className="w-full">
      <article
        className="lg:max-w-[1280px] mx-auto"
        itemScope
        itemType="https://schema.org/TechArticle"
      >
        <div className="justify-between flex px-4">
          <h1 itemProp="name" className="text-xl font-bold">{postData.title}</h1>
          {postData.isGitlab && (
            <a href={postData.repository}>
              <Image
                alt=""
                src="/images/gitlab-logo.svg"
                width={25}
                height={25}
              />
            </a>
          )}
        </div>
        <div className="mt-2 mb-4 px-4 text-xs text-gray-500">
          <Date dateString={postData.date} itemProp="dateCreated" />
        </div>
        <hr />
        <div
          className="lg:max-w-[1280px] mt-20"
          dangerouslySetInnerHTML={{ __html: postData.contentHtml }}
          itemProp="articleBody"
        />
      </article>
    </main>
  );
}