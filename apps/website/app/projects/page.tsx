import React from "react";
import Link from "next/link";
import Image from "next/image";
import { getSortedProjectsData } from "../../lib/projects";

export default async function Projects() {
  const allProjectsData = getSortedProjectsData();

  return (
    <main>
      <div className="lg:max-w-[1280px] mx-auto">
        <h3 className="font-bold text-2xl text-center">Projects</h3>
        <div className="mt-4">
          <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
            {allProjectsData.map((projectData, idx: number) => {
              return (
                <div key={idx} className="my-3 py-2 px-3 border shadow-lg">
                  <div className="h-100">
                    {projectData.thumbnail && (
                      <Image
                        alt=""
                        src={projectData.thumbnail}
                        className="card-img-top"
                      />
                    )}
                    <div className="p-2">
                      <div className="flex flex-row justify-between">
                        <Link href={projectData.articleUrl || ""}>
                          <h5 className="text-lg">{projectData.title}</h5>
                        </Link>
                        {projectData.isGitlab && (
                          <div>
                            <a href={projectData.repository}>
                              <Image
                                alt=""
                                src="/images/gitlab-logo.svg"
                                width={25}
                                height={25}
                              />
                            </a>
                          </div>
                        )}
                      </div>

                      <p className="text-gray-600 pt-4 mb-8 text-sm">
                        {projectData.description}
                      </p>
                      <div className="">
                        {projectData.chips.map((chipData, chipIdx) => {
                          return (
                            <div
                              key={chipIdx}
                              className="inline-block rounded-full bg-blue-100 py-2 px-4 m-1 text-xs"
                            >
                              {chipData.value}
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </main>
  );
}

/*
// get all posts with the tag "side-project"
export async function getStaticProps() {
  const allProjectsData = getSortedProjectsData();

  return {
    props: {
      allProjectsData,
    },
  };
}
*/
